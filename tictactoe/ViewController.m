//
//  ViewController.m
//  tictactoe
//
//  Created by Click Labs135 on 9/28/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *whoseTurn;




@property (strong, nonatomic) IBOutlet UIButton *resetButton;


@end

@implementation ViewController
UIImageView *view1;
UIImageView *view2;
UIImageView *view3;
UIImageView *view4;
UIImageView *view5;
UIImageView *view6;
UIImageView *view7;
UIImageView *view8;
UIImageView *view9;
UIAlertView *someonewon;

UIButton *button1;
UIButton *button2;
UIButton *button3;
UIButton *button4;
UIButton *button5;
UIButton *button6;
UIButton *button7;
UIButton *button8;
UIButton *button9;



UIImageView *image1;
UIImageView *image2;
UILabel *whoseTurn;
NSInteger *play;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   
    
    image1=[UIImage imageNamed:@"/Users/clicklabs135/Desktop/tictactoe/tictactoe/zero.png"];
    image2=[UIImage imageNamed:@"/Users/clicklabs135/Desktop/tictactoe/tictactoe/cross.png"];
    // set the player to 1
    play = 1;
    // update the label
    whoseTurn.text =@"X goes first";
}


- (void) updatePlayerInfo{
    
    if(play == 1) {
        play = 2;
        whoseTurn.text = @"It is O turn";
        NSLog(@"playerToken = %d", play);
    }
    else if(play == 2) {
        play = 1;
        whoseTurn.text =@"It is X turn";
        NSLog(@"playerToken = %d", play);
    }
}
//Add the action for the reset button:
- (IBAction)buttonReset:(UIButton *)sender {
    [self resetButton];
}
//Add the implementation for the “resetBoard” method:
-(void) resetBoard{
    /// clear the images stored in the UIIMageView
     view1.image = NULL;
    view2.image = NULL;
    view3.image = NULL;
    view4.image = NULL;
    view5.image = NULL;
    view6.image = NULL;
    view7.image = NULL;
    view8.image = NULL;
    view9.image = NULL;
    
    // reset the player and update the label text
    play= 1;
    whoseTurn.text = @"X goes first";
}
/*Next, implement the game logic for the touching of the spaces on the
board. */
// the touch event for the tic tac toe game
- (void)touchesBegan:(NSSet *)touches
           withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    
    // check to see which UIImage view was touched
    if(CGRectContainsPoint([view1 frame], [touch
                                        locationInView:self.view])){
        if(play==1){ view1.image = image1; }
        if(play==2){ view1.image = image2; }
    }
    
    if(CGRectContainsPoint([view2 frame], [touch
                                        locationInView:self.view])){
        if(play==1){ view2.image = image1; }
        if(play==2){ view2.image = image2; }
    }
    
    if(CGRectContainsPoint([view3 frame], [touch
                                        locationInView:self.view])){
        if(play==1){ view3.image = image1; }
        if(play==2){ view3.image = image2; }
    }
    
    if(CGRectContainsPoint([view4 frame], [touch
                                        locationInView:self.view])){
        if(play){ view4.image = image1; }
        if(play==2){ view4.image = image2; }
    }
    
    if(CGRectContainsPoint([view5 frame], [touch
                                        locationInView:self.view])){
        if(play){ view5.image = image1; }
        if(play==2){ view5.image = image2; }
    }
    
    
    if(CGRectContainsPoint([view6 frame], [touch
                                        locationInView:self.view])){
        if(play==1){ view6.image = image1; }
        if(play==2){ view6.image = image2; }
    }
    
    if(CGRectContainsPoint([view7 frame], [touch
                                        locationInView:self.view])){
        if(play==1){ view7.image = image1; }
        if(play==2){ view7.image = image2; }
    }
    
    if(CGRectContainsPoint([view8 frame], [touch
                                        locationInView:self.view])){
        if(play==1){ view8.image = image1; }
        if(play==2){ view8.image = image2; }
    }
    
    if(CGRectContainsPoint([view9 frame], [touch
                                        locationInView:self.view])){
        if(play){ view9.image = image1; }
        if(play){ view9.image = image2; }
    }
    [self updatePlayerInfo];
    
}
/* Now run the game and see what happens. It should work but there is
no check for a winner yet. Add the following method implementation to
check for a win: */
// method that will check to see if someone has won
//returns TRUE if someone wins
-(BOOL) checkForWin {
    // HORIZONTAL WINS
    if((view1.image == view2.image) & (view2.image ==
                                 view3.image) & (view1.image != NULL))
    {
        return YES;
    }
    if((view4.image == view5.image) & (view5.image ==
                                 view6.image) & (view4.image != NULL))
    {
        return YES;
    }
    if((view7.image == view8.image) & (view8.image ==
                                 view9.image) & (view7.image != NULL))
    {
        return YES;
    }
    // VERTICAL WINS
    if((view1.image == view4.image) & (view4.image ==
                                 view7.image) & (view1.image != NULL))
    {
        return YES;
    }
    if((view2.image == view5.image) & (view5.image ==
                                 view8.image) & (view2.image != NULL))
    {
        return YES;
    }
    if((view3.image == view6.image) & (view6.image ==
                                 view9.image) & (view3.image != NULL))
    {
        return YES; 
    }
    // DIAGONAL WINS
    if((view1.image == view5.image) & (view5.image ==
                                 view9.image) & (view1.image != NULL))
    {
        return YES;
    }
    if((view3.image == view5.image) & (view5.image ==
                                 view7.image) & (view3.image != NULL))
    {
        return YES;
    }
    return NO;
}
/* You now have to check for the win someplace. You could do it in
updatePlayerInfo method. If so you could put something like this in
that method: */

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
